package com.hybird.lvgl.voicerecognizeview;

import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.Region;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;

import androidx.annotation.Nullable;


public class MieEatView extends View {

    Paint bgPain = new Paint();
    Paint bodyPaint = new Paint();
    private Paint headPaint = new Paint();
    private Paint earsPaint = new Paint();
    private Paint eyePaint = new Paint();
    private Paint cakePaint = new Paint();

    private boolean eyeState = true;
    private int count = 0;
    private float curValue = 0;
    private ValueAnimator valueAnimator;

    public MieEatView(Context context) {
        super(context);
        initBase();
    }


    public MieEatView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initBase();
    }


    private void initBase() {
        bgPain.setColor(Color.parseColor("#BD71CA"));
        bgPain.setStyle(Paint.Style.FILL);
        bodyPaint.setColor(Color.RED);
        bodyPaint.setStyle(Paint.Style.FILL);
        bodyPaint.setAntiAlias(true);

        headPaint.setStyle(Paint.Style.FILL);
        headPaint.setColor(Color.parseColor("#653366"));
        headPaint.setAntiAlias(true);

        earsPaint.setStyle(Paint.Style.FILL);
        earsPaint.setColor(Color.parseColor("#653366"));
        earsPaint.setAntiAlias(true);

        eyePaint.setStyle(Paint.Style.FILL);
        eyePaint.setColor(Color.WHITE);
        eyePaint.setAntiAlias(true);

        cakePaint.setStyle(Paint.Style.FILL);
        cakePaint.setColor(Color.parseColor("#F19ECA"));
        cakePaint.setAntiAlias(true);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int width = getWidth();
        int height = getHeight();
        int w = Math.min(width, height);
        int paddingX = (width - w) / 2;
        int paddingY = (height - w) / 2;
        RectF rect = new RectF(paddingX, paddingY, w + paddingX, w + paddingY);
        canvas.drawRect(rect, bgPain);

        int bodyRadius = (int) (w * 0.2);
        int bodyCenterX = (int) (w * 0.45) + paddingX;
        int bodyCenterY = (int) (w * 0.15) + paddingY;
        int r = (int) (bodyRadius * 0.13);

        //腿
        int legWidth = (int) (bodyRadius * 0.2);
        int legHeight = (int) (bodyRadius * 0.8);

        int legBaseX = (int) (bodyCenterX + legWidth * 3.2);
        int lgeBaseY = (int) (bodyCenterY + bodyRadius * 1.7);
        RectF leg1 = new RectF(legBaseX, lgeBaseY, legBaseX + legWidth, lgeBaseY + legHeight);
        RectF leg2 = new RectF(legBaseX + legWidth, lgeBaseY, legBaseX + legWidth + legWidth, lgeBaseY + legHeight);
        RectF leg3 = new RectF(legBaseX + legWidth * 2, lgeBaseY, legBaseX + legWidth + legWidth * 2, lgeBaseY + legHeight);
        RectF leg4 = new RectF(legBaseX + legWidth * 3, lgeBaseY, legBaseX + legWidth + legWidth * 3, lgeBaseY + legHeight);
        earsPaint.setColor(Color.parseColor("#401840"));
        canvas.drawRoundRect(leg1, 40, 40, earsPaint);
        earsPaint.setColor(Color.parseColor("#6F406F"));
        canvas.drawRoundRect(leg2, 40, 40, earsPaint);
        earsPaint.setColor(Color.parseColor("#401840"));
        canvas.drawRoundRect(leg3, 40, 40, earsPaint);
        earsPaint.setColor(Color.parseColor("#6F406F"));
        canvas.drawRoundRect(leg4, 40, 40, earsPaint);

        //身体
        Bitmap bodyBitmap = Bitmap.createBitmap(bodyRadius * 2, bodyRadius * 2, Bitmap.Config.ARGB_8888);
        Canvas bodyCanvas = new Canvas();
        bodyCanvas.setBitmap(bodyBitmap);
        //调试显示背景
//        bodyPaint.setColor(Color.BLUE);
//        bodyCanvas.drawRect(0, 0, bodyBitmap.getWidth(), bodyBitmap.getHeight(), bodyPaint);
        bodyCanvas.clipRect(0, 0, r * 2.0f, bodyRadius * 2, Region.Op.DIFFERENCE);//设置显示范围
        bodyPaint.setColor(Color.parseColor("#FFCC66"));
        for (int i = 0; i < 10; i++) {
            bodyCanvas.save();
            bodyCanvas.rotate(i * 18, bodyRadius, bodyRadius);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                bodyCanvas.drawRoundRect(r, bodyRadius - r, 2 * bodyRadius - r, bodyRadius + r, r * 2, r * 2, bodyPaint);
            }
            bodyCanvas.restore();
        }
        canvas.drawBitmap(bodyBitmap, bodyCenterX, bodyCenterY, bodyPaint);

        //耳朵
        int earWidth = (int) (bodyRadius * 0.3);
        int earHeight = (int) (bodyRadius * 0.8);

        int leftEarX = (int) (bodyCenterX - bodyRadius * 0.04 - earWidth);
        int leftEarY = (int) (bodyCenterY + bodyRadius * 0.8);
        int rightEarX = (int) (bodyCenterX - bodyRadius * 0.04);
        int rightEarY = (int) (bodyCenterY + bodyRadius * 0.8);

        RectF earLeftR = new RectF(leftEarX, leftEarY, leftEarX + earWidth, leftEarY + earHeight);
        RectF earRightR = new RectF(rightEarX, rightEarY, rightEarX + earWidth, rightEarY + earHeight);

        earsPaint.setColor(Color.parseColor("#401840"));
        canvas.drawRoundRect(earLeftR, 40, 40, earsPaint);
        earsPaint.setColor(Color.parseColor("#6F406F"));
        canvas.drawRoundRect(earRightR, 40, 40, earsPaint);

        //头
        int baseW = 150 * w / 800;
        int headWidth = (int) (baseW - curValue * baseW * 0.1);
        int headHeight = (int) (90 * w / 800 + curValue * 30);
        int headRightBaseX = (int) (bodyCenterX + bodyRadius * .25);
        int headRightBaseY = (int) (bodyCenterY + bodyRadius * 0.65);
        @SuppressLint("DrawAllocation")
        Path headPath = new Path();
        headPath.moveTo(headRightBaseX, headRightBaseY);
        headPath.lineTo(headRightBaseX, headRightBaseY + headHeight);
        headPath.lineTo(headRightBaseX - headWidth, headRightBaseY + headHeight);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            headPath.addArc(headRightBaseX - headWidth - headHeight / 2,
                    headRightBaseY,
                    headRightBaseX - headWidth + headHeight / 2,
                    headRightBaseY + headHeight, 90, 180
            );
        }
        headPath.lineTo(headRightBaseX, headRightBaseY);
        canvas.drawPath(headPath, headPaint);

        //眼睛
        int eyeCenterX = (int) (bodyCenterX - bodyRadius * 0.09);
        int eyeCenterY = (int) (bodyCenterY + bodyRadius * 0.88);

        int bigEyeRadius = (int) (bodyRadius * .13);
        int offset = (int) (bigEyeRadius * .8);
        int smallEyeRadius = (int) (bodyRadius * .06);
        eyeState = System.currentTimeMillis() / 800 % 2 == 0;
        if (eyeState) {
            eyePaint.setColor(Color.WHITE);
            canvas.drawCircle(eyeCenterX - offset, eyeCenterY, bigEyeRadius, eyePaint);
            canvas.drawCircle(eyeCenterX + offset, eyeCenterY, bigEyeRadius, eyePaint);
            eyePaint.setColor(Color.parseColor("#3E003E"));
            offset = (int) (bigEyeRadius * .6);
            canvas.drawCircle(eyeCenterX - offset, eyeCenterY, smallEyeRadius, eyePaint);
            canvas.drawCircle(eyeCenterX + offset, eyeCenterY, smallEyeRadius, eyePaint);
        } else {
            Path eyeClose = new Path();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                eyeClose.addRoundRect(0, 0, bigEyeRadius * 2, (float) (bigEyeRadius * .8), bigEyeRadius, bigEyeRadius, Path.Direction.CCW);
            }
            Matrix eyeTransform = new Matrix();
            eyeTransform.postTranslate(eyeCenterX - bigEyeRadius, (float) (eyeCenterY - bigEyeRadius * 0.4));
            eyeClose.transform(eyeTransform);
            eyePaint.setColor(Color.parseColor("#3E003E"));
            canvas.drawPath(eyeClose, eyePaint);
        }


        //鼻子
        int noseCenterX = (int) (bodyCenterX - bodyRadius * 0.7 + bodyRadius * 0.05 * curValue);
        int noseCenterY = (int) (bodyCenterY + bodyRadius * 0.90);

        int boseRadius = (int) (bodyRadius * .06);
        offset = (int) (boseRadius * 1.2);
        eyePaint.setColor(Color.parseColor("#401840"));
        canvas.drawCircle(noseCenterX - offset, noseCenterY, boseRadius, eyePaint);
        canvas.drawCircle(noseCenterX + offset, noseCenterY, boseRadius, eyePaint);


        //嘴
        int mouthWidth = (int) (bodyRadius * 0.2 + curValue * bodyRadius * 0.13);
        int mouthHeight = (int) (bodyRadius * 0.1 + curValue * bodyRadius * 0.1);

        int mouthX = (int) (bodyCenterX - bodyRadius * .55 + bodyRadius * .05 * curValue);
        int mouthY = (int) (bodyCenterY + bodyRadius * 1.1);
        @SuppressLint("DrawAllocation")
        RectF mouth = new RectF(mouthX, mouthY, mouthX + mouthWidth, mouthY + mouthHeight);
        earsPaint.setColor(Color.parseColor("#3E003E"));
        canvas.drawRoundRect(mouth, 40, 40, earsPaint);

        //蛋糕
        int cakeStartBaseX = (int) (bodyCenterX - bodyRadius * .7);
        int cakeStartBaseY = (int) (bodyCenterY + bodyRadius * 1.9);

        int cakeTopRectStartBaseX = (int) (cakeStartBaseX - bodyRadius * 0);
        int cakeTopRectStartBaseY = (int) (cakeStartBaseY + bodyRadius * .0);


        cakePaint.setColor(Color.parseColor("#F19ECA"));
        double frac = 0.65;
        int cakeTopRoundRectWidth = (int) (bodyRadius * frac);
        int cakeTopRoundRectHeight = (int) (cakeTopRoundRectWidth / frac * .25);
        RectF topRoundRect = new RectF(cakeTopRectStartBaseX, cakeTopRectStartBaseY, cakeTopRectStartBaseX + cakeTopRoundRectWidth, cakeTopRectStartBaseY + cakeTopRoundRectHeight);
        Path cakeTopRoundRectPath = new Path();
        int cakeTopRoundRectCorner = cakeTopRoundRectHeight / 2;
        cakeTopRoundRectPath.moveTo(cakeTopRectStartBaseX, cakeTopRectStartBaseY);
        cakeTopRoundRectPath.addRoundRect(topRoundRect, cakeTopRoundRectCorner, cakeTopRoundRectCorner, Path.Direction.CCW);
//        canvas.drawPath(cakeTopRoundRectPath, cakePaint);

        int redDotX = (int) (cakeTopRectStartBaseX + cakeTopRoundRectWidth * 0.4);
        int redDotY = (int) (cakeTopRectStartBaseY + cakeTopRoundRectWidth * .23);
        cakePaint.setColor(Color.parseColor("#C81652"));
        canvas.drawCircle(redDotX, redDotY, (float) (cakeTopRoundRectWidth * 0.15), cakePaint);

//        cakePaint.setColor(Color.parseColor("#CB006B"));
        int cakeTopV1StartBaseX = (int) (cakeTopRectStartBaseX + cakeTopRoundRectWidth / frac * .05);
        int cakeTopV1StartBaseY = (int) (cakeTopRectStartBaseY + cakeTopRoundRectWidth / frac * .1);
        int cakeTopV1Width = (int) (cakeTopRoundRectWidth / frac * .07);
        int cakeTopV1Height = (int) (cakeTopRoundRectWidth / frac * 0.16);
        Path cakeTopV1Path = createRoundRectPath(cakeTopV1StartBaseX, cakeTopV1StartBaseY, cakeTopV1Width, cakeTopV1Height, cakeTopV1Height / 2);
//        canvas.drawPath(cakeTopV1Path, cakePaint);


        int cakeTopV2StartBaseX = (int) (cakeTopV1StartBaseX + cakeTopV1Width);
        int cakeTopV2StartBaseY = (int) (cakeTopV1StartBaseY + cakeTopRoundRectWidth / frac * .09);
        int cakeTopV2Width = (int) (cakeTopV1Width * 1);
        int cakeTopV2Height = (int) (cakeTopRoundRectWidth / frac * 0.25);
        Path cakeTopV2Path = createRoundRectPath(cakeTopV2StartBaseX, cakeTopV2StartBaseY, cakeTopV2Width, cakeTopV2Height, cakeTopV2Height / 2);
//        canvas.drawPath(cakeTopV2Path, cakePaint);

        int cakeTopV3StartBaseX = (int) (cakeTopV2StartBaseX + cakeTopV2Width);
        int cakeTopV3StartBaseY = (int) (cakeTopV1StartBaseY + cakeTopRoundRectWidth / frac * .07);
        int cakeTopV3Width = (int) (cakeTopV1Width * 1.2);
        int cakeTopV3Height = (int) (cakeTopRoundRectWidth / frac * 0.13);
        Path cakeTopV3Path = createRoundRectPath(cakeTopV3StartBaseX, cakeTopV3StartBaseY, cakeTopV3Width, cakeTopV3Height, cakeTopV3Height / 2);
//        canvas.drawPath(cakeTopV3Path, cakePaint);


        int cakeTopV4StartBaseX = (int) (cakeTopV3StartBaseX + cakeTopV3Width);
        int cakeTopV4StartBaseY = (int) (cakeTopV1StartBaseY + cakeTopRoundRectWidth / frac * .07);
        int cakeTopV4Width = (int) (cakeTopRoundRectWidth / frac * .055);
        int cakeTopV4Height = (int) (cakeTopRoundRectWidth / frac * 0.3);
        Path cakeTopV4Path = createRoundRectPath(cakeTopV4StartBaseX, cakeTopV4StartBaseY, cakeTopV4Width, cakeTopV4Height, cakeTopV4Height / 2);
//        canvas.drawPath(cakeTopV4Path, cakePaint);

        int cakeTopV5StartBaseX = (int) (cakeTopV4StartBaseX + cakeTopV4Width);
        int cakeTopV5StartBaseY = (int) (cakeTopV4StartBaseY - cakeTopRoundRectWidth / frac * .02);
        Path cakeTopV5Path = createRoundRectPath(cakeTopV5StartBaseX, cakeTopV5StartBaseY, cakeTopV3Width, cakeTopV3Height, cakeTopV3Height / 2);
//        canvas.drawPath(cakeTopV5Path, cakePaint);

        int cakeTopV6StartBaseX = (int) (cakeTopV5StartBaseX + cakeTopV3Width);
        int cakeTopV6StartBaseY = (int) (cakeTopV5StartBaseY);
        Path cakeTopV6Path = createRoundRectPath(cakeTopV6StartBaseX, cakeTopV6StartBaseY, cakeTopV3Width, (float) (cakeTopV3Height * 1.3), cakeTopV3Height);
//        canvas.drawPath(cakeTopV6Path, cakePaint);

        cakeTopRoundRectPath.op(cakeTopV1Path, Path.Op.UNION);
        cakeTopRoundRectPath.op(cakeTopV2Path, Path.Op.DIFFERENCE);
        cakeTopRoundRectPath.op(cakeTopV3Path, Path.Op.UNION);
        cakeTopRoundRectPath.op(cakeTopV4Path, Path.Op.DIFFERENCE);
        cakeTopRoundRectPath.op(cakeTopV5Path, Path.Op.UNION);
        cakeTopRoundRectPath.op(cakeTopV6Path, Path.Op.DIFFERENCE);

        int starRadius = (int) (cakeTopRoundRectWidth / frac * 0.2);
        int starSmallRadius = (int) (starRadius * .2);
        int starBaseX = cakeTopRectStartBaseX + cakeTopRoundRectWidth;
        int starBaseY = (int) (cakeTopRectStartBaseY + cakeTopRoundRectHeight * 0.4);
        Path starPath = new Path();

        for (int i = 0; i < 6; i++) {
            starPath.moveTo(0, 0);
            starPath.lineTo(0, starSmallRadius);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                starPath.addArc(starRadius - 2 * starSmallRadius, 0, starRadius, starSmallRadius * 2, -90, 180);
            }
            Matrix matrixRotate = new Matrix();
            matrixRotate.postRotate(-22.5f, 0, 0);
            starPath.transform(matrixRotate);
            starPath.lineTo(0, 0);
        }
        starPath.lineTo(0, 0);
        starPath.close();
        Matrix matrixTransform = new Matrix();
        matrixTransform.postRotate(170, 0, 0);
        matrixTransform.postTranslate((float) (starBaseX - starSmallRadius * 1.3), (float) (starBaseY + cakeTopRoundRectWidth * 0.04));
        starPath.transform(matrixTransform);

//        canvas.drawPath(starPath, cakePaint);
//        cakeTopRoundRectPath.op(starPath, Path.Op.DIFFERENCE);

        int cakeWidth = (int) (cakeTopRoundRectWidth * 0.8);
        int cakeHeight = cakeWidth;
        int cakeDiff = cakeTopRoundRectWidth - cakeWidth;
        int cakeCorner = (int) (cakeHeight * 0.2);
        RectF cakeRect = new RectF(0, 0, cakeWidth, cakeHeight);

        Path finalTopShape = new Path();
        finalTopShape.moveTo(0, 0);
        finalTopShape.addRoundRect(cakeRect, cakeCorner, cakeCorner, Path.Direction.CCW);
        Matrix matrixShapeTransform = new Matrix();
        matrixShapeTransform.postTranslate(cakeTopRectStartBaseX + cakeDiff / 2, (float) (cakeTopRectStartBaseY + cakeDiff * 1.1));
        finalTopShape.transform(matrixShapeTransform);

        cakePaint.setColor(Color.parseColor("#CF6D40"));
        finalTopShape.op(starPath, Path.Op.DIFFERENCE);
        canvas.drawPath(finalTopShape, cakePaint);

        cakeTopRoundRectPath.op(finalTopShape, Path.Op.INTERSECT);
        cakeTopRoundRectPath.op(starPath, Path.Op.DIFFERENCE);
        cakePaint.setColor(Color.parseColor("#F19ECA"));
        canvas.drawPath(cakeTopRoundRectPath, cakePaint);

        //红线
        cakePaint.setColor(Color.parseColor("#C81652"));
        int redLineStartX = cakeTopRectStartBaseX + cakeDiff / 2;
        int redLineStartY = (int) (cakeTopRectStartBaseY + cakeDiff * 2.8);
        int redLineWidth = (int) (cakeDiff * .3);
        canvas.drawRect(redLineStartX, redLineStartY, redLineStartX + cakeWidth, redLineStartY + redLineWidth, cakePaint);

        int cakeDot1X = (int) (cakeTopRectStartBaseX - cakeHeight * 0.1);
        int cakeDot1Y = (int) (cakeTopRectStartBaseY + cakeHeight * 1.2);
        cakePaint.setColor(Color.parseColor("#CF6D40"));
        canvas.drawCircle(cakeDot1X, cakeDot1Y, (float) (cakeDiff * 0.25), cakePaint);

        int cakeDot2X = (int) (cakeTopRectStartBaseX + cakeWidth * 0.35);
        int cakeDot2Y = (int) (cakeTopRectStartBaseY + cakeHeight * 1.1);
        cakePaint.setColor(Color.parseColor("#9F4040"));
        canvas.drawCircle(cakeDot2X, cakeDot2Y, (float) (cakeDiff * 0.3), cakePaint);

        int cakeDot3X = (int) (cakeTopRectStartBaseX + cakeWidth * 0.55);
        int cakeDot3Y = (int) (cakeTopRectStartBaseY + cakeHeight * 1.05);
        cakePaint.setColor(Color.parseColor("#9F4040"));
        canvas.drawCircle(cakeDot3X, cakeDot3Y, (float) (cakeDiff * 0.2), cakePaint);

        int cakeDot4X = (int) (cakeTopRectStartBaseX + cakeWidth * 0.85);
        int cakeDot4Y = (int) (cakeTopRectStartBaseY + cakeHeight * 1.1);
        cakePaint.setColor(Color.parseColor("#9F4040"));
        canvas.drawCircle(cakeDot4X, cakeDot4Y, (float) (cakeDiff * 0.25), cakePaint);


        int cakeDot5X = (int) (cakeTopRectStartBaseX + cakeHeight * 1.3);
        int cakeDot5Y = (int) (cakeTopRectStartBaseY + cakeHeight * 1.2);
        cakePaint.setColor(Color.parseColor("#CF6D40"));
        canvas.drawCircle(cakeDot5X, cakeDot5Y, (float) (cakeDiff * 0.25), cakePaint);

        int cakeDot6X = (int) (cakeTopRectStartBaseX + cakeWidth * 1.5);
        int cakeDot6Y = (int) (cakeTopRectStartBaseY + cakeHeight * 1.2);
        cakePaint.setColor(Color.parseColor("#CF6D40"));
        canvas.drawCircle(cakeDot6X, cakeDot6Y, (float) (cakeDiff * 0.2), cakePaint);


        //嘴里掉出来的草料碎屑
        if (count % 2 == 0) {
            int dot1X = (int) (mouthX + bodyRadius * 0.1 + bodyRadius * 0.05 * curValue);
            int dot1Y = (int) (bodyCenterY + bodyRadius * 1.2 + curValue * curValue * bodyRadius * 0.8);
            cakePaint.setColor(Color.parseColor("#CF6D40"));
            canvas.drawCircle(dot1X, dot1Y, (float) (cakeDiff * 0.25), cakePaint);

            int dot2X = (int) (mouthX + bodyRadius * 0.1 - bodyRadius * 0.05 * curValue);
            int dot2Y = (int) (bodyCenterY + bodyRadius * 1.2 + curValue * bodyRadius * 0.8);
            cakePaint.setColor(Color.parseColor("#CF6D40"));
            canvas.drawCircle(dot2X, dot2Y, (float) (cakeDiff * 0.25), cakePaint);
        } else {
            int dot3X = (int) (mouthX + bodyRadius * 0.2 - bodyRadius * 0.1 * curValue);
            int dot3Y = (int) (bodyCenterY + bodyRadius * 1.2 + curValue * bodyRadius * 0.8);
            cakePaint.setColor(Color.parseColor("#CF6D40"));
            canvas.drawCircle(dot3X, dot3Y, (float) (cakeDiff * 0.25), cakePaint);
        }
        count++;
    }

    private Path createRoundRectPath(float cakeTopV1StartBaseX, float cakeTopV1StartBaseY, float cakeTopV1Width, float cakeTopV1Height, int cakeTopV1Corner) {
        RectF cakeTopV1Rect = new RectF(cakeTopV1StartBaseX, cakeTopV1StartBaseY, cakeTopV1StartBaseX + cakeTopV1Width, cakeTopV1StartBaseY + cakeTopV1Height);
        Path cakeTopV1Path = new Path();
        cakeTopV1Path.addRoundRect(cakeTopV1Rect, cakeTopV1Corner, cakeTopV1Corner, Path.Direction.CCW);
        return cakeTopV1Path;
    }

    private void createAnimation() {
        if (valueAnimator == null) {
            valueAnimator = ValueAnimator.ofFloat(0f, 1f);
            valueAnimator.setDuration(700);
            valueAnimator.setRepeatCount(ValueAnimator.INFINITE);
            valueAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
            valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                int count = 0;

                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    count++;
                    if (count % 4 == 1) {
                        curValue = (float) animation.getAnimatedValue();
                        postInvalidate();
                    }
                }
            });
        } else {
        }

        valueAnimator.start();
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        createAnimation();
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (valueAnimator != null) {
            valueAnimator.cancel();
        }
    }
}
